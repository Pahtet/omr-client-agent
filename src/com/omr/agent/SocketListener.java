package com.omr.agent;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.omr.data.Command;
import com.omr.data.CommandsEnum;

public class SocketListener implements Runnable
{
	static Log log = LogFactory.getLog(SocketListener.class);
	private ServerSocket socket;
	private boolean io;	
	private PeriodicTask periodicTask;
	
	SocketListener(int port)
	{
		try
		{
			socket = new ServerSocket(port);
			io = false;
		} 
		catch (IOException e)
		{
			io = true;
			log.debug(e);			
		}	
	}
	
	public void run()
	{
		while (io == false)
		{
			try
			{
				handleConnection(socket.accept());
			}
			catch (IOException e)
			{
				log.debug(e);
			}
		}
	}
	
	private void handleConnection(Socket s) throws IOException
	{
		log.info("Admin connected "+s.getInetAddress().toString());
		ObjectInputStream in = new ObjectInputStream(s.getInputStream());
		ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());		
		boolean localIO = false;
		try
		{
			while (localIO == false)
			{
				Command c = (Command)in.readObject();
				log.info("Commad from Admin "+c.getCommand().toString());
				switch(c.getCommand())
				{
					case ACTIVATE:
						if (null == periodicTask)
						{
							periodicTask = new PeriodicTask();
							periodicTask.StartTask((int)c.getData());
						}
						break;
					case DEACTIVATE:
						if (null != periodicTask)
						{
							periodicTask.StopTask();
							periodicTask = null;
						}
						break;
					case GETMEMORYUSAGE:
						long mem = TaskListWrapper.getCurrentuMemory();
						log.info("Memory Usage "+mem/1024+"MB");
						sendCommad(out, new Command(CommandsEnum.MEMORYUSAGE, new Long(mem)));
						break;						
					case CONNECT:
						sendCommad(out, new Command(CommandsEnum.VERSION, CurrentInfo.getVersion()));
						break;					
					default:
						log.warn("Unknow command");
						break;
				}
			}
		}
		catch(IOException | ClassNotFoundException e)
		{
			localIO = true;
			log.debug(e);
		}
	}
	
	private static void sendCommad(ObjectOutputStream out, Command c) throws IOException
	{
			out.writeObject(c);
			out.flush();		
	}
}