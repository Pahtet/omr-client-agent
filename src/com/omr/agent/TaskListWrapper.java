package com.omr.agent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.omr.data.ProcessInfo;

class TaskListWrapper
{
	private Process cmd;
	private BufferedReader reader;
	private OsEnum Os = CurrentInfo.getOs();	
	
	TaskListWrapper()
	{
		if (Os.equals(OsEnum.UNKNOWN))
		{
			System.out.println("This os is not supported!");
			System.exit(-1);
		}
	}
	
	public ArrayList<ProcessInfo> getProcessInfo() 
	{
		if (Os.equals(OsEnum.UNKNOWN))
			return null;
		
		ArrayList<ProcessInfo> ans = new ArrayList<ProcessInfo>();
		initConsole();	
		
		String line;
		try
		{
			while ( (line = reader.readLine()) != null)
			{
				ProcessInfo process = new ProcessInfo();
				if (Os.equals(OsEnum.WIN))
					process.parseTaskList(line);
				else if (Os.equals(OsEnum.UNIX))
					process.parseProcessStatus(line);
				if (process.isValid())				
					ans.add(process);				
			}			
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return ans;		
	}
	
	public static long getMemory(List<ProcessInfo> processList)
	{
		long out = 0;
		for (ProcessInfo p: processList)
		{
			out += p.getMemory();
		}
		return out;
	}
	
	public static long getCurrentuMemory()
	{
		TaskListWrapper tmp = new TaskListWrapper();
		long out = 0;
		for (ProcessInfo p: tmp.getProcessInfo())
		{
			out += p.getMemory();
		}
		return out;
	}
	
	private void initConsole()
	{
		try
		{
			if (Os.equals(OsEnum.WIN))
			{
				cmd = new ProcessBuilder("tasklist.exe", "/fo", "csv", "/nh").start();			
				reader = new BufferedReader(new InputStreamReader(cmd.getInputStream(),"cp866"));			
			}
			else if (Os.equals(OsEnum.UNIX))
			{
				cmd = new ProcessBuilder("ps", "-A", "-eo", "comm,pid,tty,size").start();			
				reader = new BufferedReader(new InputStreamReader(cmd.getInputStream(),"UTF8"));		
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}	
}
	
	