package com.omr.agent;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DataBaseHandler
{
	private final static String insert = "INSERT INTO ";
	private final static String create = "CREATE TABLE IF NOT EXISTS ";
	private final static String primaryKey = " PRIMARY KEY NOT NULL,";
	private Connection c;
	
	public DataBaseHandler()
	{

	}

	public boolean createTables()
	{
		try
		{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:main.db");
			createTable(c, "measurements", "id INT", "platformId INT, codeFragmentId INT, type INT, timestampId INT, eventTypeId INT, FOREIGN KEY (codeFragmentId) REFERENCES system (systemLevelId),FOREIGN KEY (platformId) REFERENCES measurementPlatform (platformId)"); //������
			createTable(c, "eventType", "eventTypeId INT", "name TEXT, FOREIGN KEY(eventTypeId) REFERENCES measurements(eventTypeId)"); //���_���������� 
			createTable(c, "system", "systemLevelId INT", "name TEXT,type INT,systemId TEXT,FOREIGN KEY (type) REFERENCES sprLevelVichSystem (typeLevelSystemId)"); //����������
			createTable(c, "measurementType", "measurementTypeId INT", "name TEXT, FOREIGN KEY (measurementTypeId) REFERENCES measurements (measurementTypeId)"); //���_���������
			createTable(c,"sprLevelVichSystem","typeLevelSystemId INT","name TEXT"); //���_������� ����������
			createTable(c,"measurementPlatform","platformId INT","additionPlatform INT,platfomType INT,stuff INT,id INT,FOREIGN KEY (platformId) REFERENCES platformType (typePlatformId),FOREIGN KEY (stuff) REFERENCES stuff (componentId)"); //���_���������
			createTable(c,"platformType","typePlatformId INT","name TEXT"); //���_������������
			createTable(c,"stuff","componentId INT","name TEXT,vendor TEXT,FOREIGN KEY (componentId) REFERENCES measurementPlatform (stuff)"); //���_������
			c.close();
		} catch (Exception e)
		{
			System.err.println(e.getMessage());
			return false;
		}
		return true;
	}

	private void createTable(Connection c, String tableName, String key, String content) throws SQLException
	{
		Statement stmt = c.createStatement();
		stmt.executeUpdate(create + " " + tableName + " (" + key + primaryKey + content + ")");
		stmt.close();
	}
	
	public void addMesurment(String methodName,Integer time) throws SQLException
	{
		Statement stmt = c.createStatement();
		//stmt.executeUpdate(insert + " measurements  "+ ("values  + key + primaryKey + content + ")"); //TODO:fix
		stmt.close();
	}
}
