package com.omr.agent;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.omr.data.Command;
import com.omr.data.CommandsEnum;

public class ProfilerSocketListener implements Runnable
{
	static Log log = LogFactory.getLog(ProfilerSocketListener.class);
	private ServerSocket socket;
	private boolean io;	
	
	ProfilerSocketListener(int port)
	{
		try
		{
			socket = new ServerSocket(port);
			io = false;
		} 
		catch (IOException e)
		{
			io = true;
			log.debug(e);			
		}	
	}
	
	public void run()
	{
		while (io == false)
		{
			try
			{
				handleConnection(socket.accept());
			}
			catch (IOException e)
			{
				log.debug(e);
			}
		}
	}
	
	private void handleConnection(Socket s) throws IOException
	{
		log.info("Profiler connected "+s.getInetAddress().toString());
		ObjectInputStream in = new ObjectInputStream(s.getInputStream());
		ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());		
		boolean localIO = false;
		try
		{
			while (localIO == false)
			{
				Command c = (Command)in.readObject();
				log.info("Commad from Profiler "+c.getCommand().toString());
				switch(c.getCommand())
				{
					case CODEMETRICS:
						log.info("Codemetrics command");
					default:
						log.warn("Unknow command");
						break;
				}
			}
		}
		catch(IOException | ClassNotFoundException e)
		{
			localIO = true;
			log.debug(e);
		}
	}
	
	private static void sendCommad(ObjectOutputStream out, Command c) throws IOException
	{
			out.writeObject(c);
			out.flush();		
	}
}