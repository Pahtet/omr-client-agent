package com.omr.agent;

class CurrentInfo
{
	public static OsEnum getOs()
	{
		if (isWindows())
			return OsEnum.WIN;
		else if (isUnix())
			return OsEnum.UNIX;
		else
			return OsEnum.UNKNOWN;		
	}
	
	public static Integer getVersion()
	{
		return ver;
	}
	
	private static int ver = 1;
	
	private static String OS = System.getProperty("os.name").toLowerCase();
	
	private static boolean isWindows()
	{		 
		return (OS.indexOf("win") >= 0); 
	}

	private static boolean isUnix()
	{		 
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );
	}
}