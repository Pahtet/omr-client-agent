package com.omr.agent;
import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;

public class Main
{
	static Log log = LogFactory.getLog(Main.class);
	public static Document configuration;
	public static void main(String[] args)
	{
		try
		{
			getConfig();
		} catch (Exception e)
		{
			log.error("Unable to load configuration");
			return;
		}
		log.info("OMR agent started");
		System.out.println("Data base " + new DataBaseHandler().createTables());
		SocketListener s = new SocketListener(43210);
		ProfilerSocketListener p = new ProfilerSocketListener(43211);
		new Thread(s).start();
		new Thread(p).start();
	}
	
	private static void getConfig() throws Exception
	{
		File configFile = new File("config.xml");
		if(configFile.exists())
		{
			log.info("Found configuration file");
			try
			{
				readConfig(configFile);
			} catch (Exception e)
			{
				emptyFile();
			}
		}
		else
		{
			emptyFile();
		}
	}
	
	private static void readConfig(File f) throws Exception
	{
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		configuration = dBuilder.parse(f);
	}
	
	private static Document emptyFile() throws ParserConfigurationException
	{
		return DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
	}
}