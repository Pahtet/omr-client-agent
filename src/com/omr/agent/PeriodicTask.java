package com.omr.agent;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.omr.data.ProcessInfo;

public class PeriodicTask
{
	static Log log = LogFactory.getLog(PeriodicTask.class);
	private ArrayList<ArrayList<ProcessInfo>> pInfo = new ArrayList<ArrayList<ProcessInfo>>();
	private TaskListWrapper tlWrapper = new TaskListWrapper();
	private Timer timer;
	
	public void StartTask(int sec)
	{
		log.info("start");
		timer = new Timer();
		timer.schedule(new ExTimer(), 0L, (long)sec * 1000L);
	}
	
	public void StopTask()
	{
		timer.cancel();
		log.info("stop");
	}
	
	private class ExTimer extends TimerTask
	{
		@Override
	    public void run()
		{
			ArrayList<ProcessInfo> list = tlWrapper.getProcessInfo();
			pInfo.add(list);
			log.info("Run. Found "+list.size()+" process");
			fileWrite(list);
	    }	
		
		private void fileWrite(ArrayList<ProcessInfo> list) 
		{
			String fileName;			
			SimpleDateFormat sdf=new SimpleDateFormat("YYYY_MM_dd");
			File folder=new File("Reports/"+sdf.format(new Date()));
			if(!folder.exists())
			{
				folder.mkdirs();
			}
			sdf=new SimpleDateFormat("HH_mm_ss");
			fileName=folder.getAbsolutePath()+'/'+sdf.format(new Date())+"_report.txt";
			log.info("Wrintg to file "+fileName);
			FileOutputStream outFile = null;		   
		    try 
		    {
		    	outFile = new FileOutputStream(fileName);		    	
			}
		    catch (FileNotFoundException e) 
		    {				
				e.printStackTrace();
			} 	
		    
		    if (outFile != null)
		    {
			    PrintWriter pw = new PrintWriter(outFile);
			    
			    for (ProcessInfo p: list)
			    {
			    	pw.print(p.getProcessName()+",");			 
			    	pw.print(p.getPid()+",");
			    	pw.print(p.getTty()+",");			
			    	pw.print(p.getMemory()+",");
			    	pw.println();
			    }	
			    pw.flush();
			    
			    try 			    
			    {				    	
					outFile.close();
				}
			    catch (IOException e)
			    {					
					e.printStackTrace();
				}
		    }		    
		}
	}		
}
