package com.omr.agent;

import java.io.Serializable;

public enum OsEnum implements Serializable
{
	WIN, UNIX, UNKNOWN;
}